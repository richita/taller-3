describe("Simulador de credito",function(){
	describe("Test 1",function(){
		var banco;
		var persona;
		beforeEach(function(){
    		banco = new Banco();
    		persona = new Persona(15,"Argentina");
  		});
  		it("1)Si la persona tiene 15 años y nacionalidad Argentina se le niega el prestamo.",function(){
  			var resultado = banco.EvaluarPrestamo(persona);
  			var esperado = false;
  			expect(resultado).toBe(esperado);
  		})
	})
	describe("Test 2",function(){
		var banco;
		var personaa;
		beforeEach(function(){
    		banco = new Banco();
    		persona = new Persona(32,"Chilena");
  		});
  		it("1)Si la persona tiene 32 años y nacionalidad Chilena se le da el credito.",function(){
  			var resultado = banco.EvaluarPrestamo(persona);
  			var esperado = true;
  			expect(resultado).toBe(esperado);
  		})
  		it("2)La persona pide pagar el credito en 11 meses se le acepta(maximo 12 minimo 1).",function(){
  			var resultado = banco.ValidarMeses(11);
  			var esperado = true;
  			expect(resultado).toBe(esperado);
  		})
  		it("3)La persona pide un credito de 5000000 se le acepta(maximo 9000000 minimo 2000000).",function(){
  			var resultado = banco.ValidarCredito(5000000);
  			var esperado = true;
  			expect(resultado).toBe(esperado);
  		})
  		it("4)La persona cumple con los requisitos,la cuota mensual es de 501347.",function(){
  			var resultado =Math.round(banco.CalcularCuotaMensual(11,5000000));
  			var esperado = 501347;
  			expect(resultado).toEqual(esperado);
  		})
	})
	describe("Test 3",function(){
		var banco;
		var persona;
		beforeEach(function(){
    		banco = new Banco();
    		persona = new Persona(18,"Chilena");
  		});
  		it("1)Si la persona tiene 18 años y nacionalidad Chilena se le da el credito.",function(){
  			var resultado = banco.EvaluarPrestamo(persona);
  			var esperado = true;
  			expect(resultado).toBe(esperado);
  		})
  		it("2)La persona pide pagar el credito en 9 meses se le acepta(maximo 12 minimo 1).",function(){
  			var resultado = banco.ValidarMeses(9);
  			var esperado = true;
  			expect(resultado).toBe(esperado);
  		})
  		it("3)La persona pide un credito de 1000000 se le rechaza(maximo 9000000 minimo 2000000).",function(){
  			var resultado = banco.ValidarCredito(1000000);
  			var esperado = false;
  			expect(resultado).toBe(esperado);
  		})
	})
	describe("Test 4",function(){
		var banco;
		var persona;
		beforeEach(function(){
    		banco = new Banco();
    		persona = new Persona(45,"Chilena");
  		});
  		it("1)Si la persona tiene 45 años y nacionalidad Chilena se le da el credito.",function(){
  			var resultado = banco.EvaluarPrestamo(persona);
  			var esperado = true;
  			expect(resultado).toBe(esperado);
  		})
  		it("2)La persona pide pagar el credito en 34 meses se le rechaza(maximo 12 minimo 1).",function(){
  			var resultado = banco.ValidarMeses(34);
  			var esperado = false;
  			expect(resultado).toBe(esperado);
  		})
	})

});