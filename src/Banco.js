function Banco(){
}
function Persona(edad,nacionalidad) {
	this.edad = edad;
	this.nacionalidad = nacionalidad;
}
Banco.prototype.EvaluarPrestamo = function(persona){
	if(persona.edad>=18 && persona.nacionalidad.localeCompare("Chilena")==0){
		return true
	}else{
		return false
	}
}
Banco.prototype.ValidarMeses = function(meses){
	if(meses>=1 && meses<=12){
		this.meses = meses;
		return true;
	}else{
		return false;
	}
}
Banco.prototype.ValidarCredito = function(credito){
	if(credito>=2000000 && credito<=9000000){
		this.credito = credito
		return true;
	}else{
		return false;
	}
}
Banco.prototype.CalcularCuotaMensual = function(meses,credito){
	const impuesto = 0.0167;
	//a = credito*i*[(i+1)^n]
	//b = ((i+1)^n)-1
	//cuota = a/b      
	var a = credito*impuesto*Math.pow(impuesto+1,meses)
	var b = Math.pow(impuesto+1,meses) - 1

	return a/b
}